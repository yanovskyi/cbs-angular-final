import { TestBed, async, inject } from '@angular/core/testing';

import { HobbyGuard } from './hobby.guard';

describe('HobbyGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HobbyGuard]
    });
  });

  it('should ...', inject([HobbyGuard], (guard: HobbyGuard) => {
    expect(guard).toBeTruthy();
  }));
});
