import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { CanActivate, CanActivateChild, Router } from '@angular/router';
import { AuthService } from '../services'

@Injectable({
  providedIn: 'root'
})
export class HobbyGuard implements CanActivate, CanActivateChild  {
  constructor(private authService: AuthService, private router: Router) {}
  canActivate(route, state): boolean {
      if (this.authService.isLoggedIn) {
          return true;
      } else {
          this.router.navigate(['/admin']);
          return false;
      }
  }
  canActivateChild(route, state): boolean {
      return this.canActivate(route, state);
  }
}
