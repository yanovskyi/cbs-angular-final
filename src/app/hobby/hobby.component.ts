import { Component } from '@angular/core';
import { HobbyService } from '../services';
import { Hobby } from '../models';

@Component({
  selector: 'app-hobby',
  templateUrl: './hobby.component.html',
  styleUrls: ['./hobby.component.scss']
})
export class HobbyComponent {

  public hobbies: Hobby[];

  constructor(public hobbyService: HobbyService) {
    this.getHobbies();
  }

  getHobbies() {
    this.hobbyService.getHobbies()
    .then((hobbies: Hobby[]) => {
      this.hobbies = hobbies;
    })
    .catch((error) => {

    })
  }
}
