import { Component } from '@angular/core';
import { HobbyService } from '../../services';
import { Hobby } from '../../models';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent {

  public hobbies: Hobby[];

  constructor(public hobbyService: HobbyService) {
    this.getHobbies();
  }

  getHobbies() {
    this.hobbyService.getHobbies()
    .then((hobbies: Hobby[]) => {
      this.hobbies = hobbies;
    })
    .catch((error) => {

    })
  }

  addHobby() {
    this.hobbyService.addHobby('[New Hobby]')
    .then((res) => {
      this.getHobbies();
    })
    .catch((error) => {
      this.getHobbies();
    })
  }

  saveHobby(id: string, name: string) {
    this.hobbyService.saveHobby(id, name)
    .then((res) => {
      this.getHobbies();
    })
    .catch((error) => {
      this.getHobbies();
    })
  }

  deleteHobby(id: string) {
    this.hobbyService.deleteHobby(id)
    .then((res) => {
      this.getHobbies();
    })
    .catch((error) => {
      this.getHobbies();
    })
  }

}
