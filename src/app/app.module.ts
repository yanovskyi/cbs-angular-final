import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTabsModule } from '@angular/material/tabs';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import {MatInputModule} from '@angular/material/input';
import {MatSnackBarModule} from '@angular/material/snack-bar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CoursesComponent } from './courses/courses.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { HobbyComponent } from './hobby/hobby.component';
import { AdminComponent } from './admin/admin.component';
import { EditComponent } from './hobby/edit/edit.component';
import { ResoursesService, CoursesService } from './services';
import { CourseInfoComponent } from './courses/course-info/course-info.component';
import { ResoursesComponent } from './resourses/resourses.component';
import { ItvdnComponent } from './resourses/itvdn/itvdn.component';
import { ItcComponent } from './resourses/itc/itc.component';
import { DouComponent } from './resourses/dou/dou.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CoursesComponent,
    ErrorPageComponent,
    HobbyComponent,
    AdminComponent,
    EditComponent,
    CourseInfoComponent,
    ResoursesComponent,
    ItvdnComponent,
    ItcComponent,
    DouComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FlexLayoutModule,
    MatTabsModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatInputModule,
    MatSnackBarModule,
    HttpClientModule
  ],
  providers: [
    ResoursesService,
    CoursesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
