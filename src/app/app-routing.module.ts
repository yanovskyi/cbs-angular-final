import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HobbyComponent } from './hobby/hobby.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { AdminComponent } from './admin/admin.component';
import { EditComponent } from './hobby/edit/edit.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseInfoComponent } from './courses/course-info/course-info.component';
import { ResoursesComponent } from './resourses/resourses.component';
import { DouComponent } from './resourses/dou/dou.component';
import { ItcComponent } from './resourses/itc/itc.component';
import { ItvdnComponent } from './resourses/itvdn/itvdn.component';
import { HobbyGuard } from './hobby/hobby.guard'


const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'hobby', children: [
    { path: 'edit',canActivate: [HobbyGuard], component: EditComponent },
    { path: '', component: HobbyComponent }
  ]},
  { path: 'courses', component: CoursesComponent },
  { path: 'courses/:id', component: CourseInfoComponent },
  { path: 'resourses', children: [
    { path: 'itvdn', component: ItvdnComponent },
    { path: 'itc', component: ItcComponent },
    { path: 'dou', component: DouComponent },
    { path: '', component: ResoursesComponent },
  ]},
  { path: 'admin', component: AdminComponent },
  { path: 'error-page', component: ErrorPageComponent },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', component: ErrorPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
