import { Component, OnInit } from '@angular/core';
import { CoursesService } from '../services/courses.service';
import { Course } from '../models/course.model';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {

  public courses: Course[];


  constructor(coursesService: CoursesService) { 
    this.courses = coursesService.getAllCourses();
  }

  ngOnInit() {
  }

}
