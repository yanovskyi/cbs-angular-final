export interface Course {
    id: number;
    name: string;
    description: string;    
    lessons: Array<string>;
    img: string;
  }
  