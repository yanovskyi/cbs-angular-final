import { TestBed } from '@angular/core/testing';

export interface Resourse {
    id: number;
    name: string;
    page: string;
    linkToSite: string;
}