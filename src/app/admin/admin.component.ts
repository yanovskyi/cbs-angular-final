import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  constructor(public authService: AuthService) { }

  ngOnInit() {
  }

  public login(login: string, pass: string) {
    this.authService.login(login, pass);
  }

  public logout() {
    this.authService.logout();
  }

}
