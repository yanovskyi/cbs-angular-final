import { Injectable } from '@angular/core';
import { Resourse } from '../models';

@Injectable({
  providedIn: 'root'
})
export class ResoursesService {

  private resoursesOfKnowledgeList: Resourse[];

  constructor() {
    this.resoursesOfKnowledgeList = [
      {
        id: 1,
        name: 'ITVDN',
        page: 'itvdn',
        linkToSite: 'https://itvdn.com'

      },
      {
        id: 2,
        name: 'ITC',
        page: 'itc',
        linkToSite: 'https://itc.ua'

      },
      {
        id: 3,
        name: 'DOU',
        page: 'dou',
        linkToSite: 'http://dou.ua'

      }
    ]
  }

  public getAllResourses() {
    return this.resoursesOfKnowledgeList;
  }
}
