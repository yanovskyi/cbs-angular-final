import { TestBed } from '@angular/core/testing';

import { ResoursesService } from './resourses.service';

describe('ResoursesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResoursesService = TestBed.get(ResoursesService);
    expect(service).toBeTruthy();
  });
});
