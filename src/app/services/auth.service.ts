import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';

export interface User {
  userName: string;
  userPassword: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private users: User[] = [
    {
      userName: 'admin',
      userPassword: 'admin'
    }
  ];

  public currentUserName: string;
  public isLoggedIn: boolean = false;
  private currentUser: User;

  constructor(private router: Router, private _snackBar: MatSnackBar) { }

  login(login: string, pass: string) {
    this.currentUser = this.users.find((user: User) => {
      return user.userName === login && user.userPassword === pass;
    });

    if (this.currentUser) {
      this.isLoggedIn = true;
      this.currentUserName = this.currentUser.userName;
      this.router.navigate(['hobby/edit']);

    } else {
      this.isLoggedIn = false;
      this.currentUserName = '';
      this.openSnackBar('Wrong login or password!')
    }
  }

  logout() {
    this.currentUser = undefined;
    this.currentUserName = '';
    this.isLoggedIn = false;
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, '', {
      duration: 2000,
    });
  }
}
