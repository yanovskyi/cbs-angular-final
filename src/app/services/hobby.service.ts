import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, delay } from 'rxjs/operators';
import { Hobby } from '../models';

@Injectable({
  providedIn: 'root'
})
export class HobbyService {

  constructor(private http: HttpClient) { }

  getHobbies(): Promise<Hobby[]> {
    return this.http.get('http://localhost:2403/my-hobbies').pipe(map((response: Hobby[]) => {
        return response;
    })).toPromise();
  }

  addHobby(name: string) {
    return this.http.post('http://localhost:2403/my-hobbies', {name}).toPromise();
  }

  saveHobby(id: string, name: string) {
    return this.http.put(`http://localhost:2403/my-hobbies/${id}`, {name}).toPromise();
  }

  deleteHobby(id: string) {
    return this.http.delete(`http://localhost:2403/my-hobbies/${id}`).toPromise();
  }
}
