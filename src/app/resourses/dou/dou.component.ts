import { Component, OnInit } from '@angular/core';
import { ResoursesService } from '../../services';
import { Resourse } from '../../models';

@Component({
  selector: 'app-dou',
  templateUrl: './dou.component.html',
  styleUrls: ['./dou.component.scss']
})
export class DouComponent {

  public douItem: Resourse;

  constructor(resoursesService: ResoursesService) {
    this.douItem = resoursesService.getAllResourses()[2];
  }

}
