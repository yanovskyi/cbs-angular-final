import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItcComponent } from './itc.component';

describe('ItcComponent', () => {
  let component: ItcComponent;
  let fixture: ComponentFixture<ItcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
