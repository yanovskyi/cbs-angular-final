import { Component, OnInit } from '@angular/core';
import { ResoursesService } from '../../services';
import { Resourse } from '../../models';

@Component({
  selector: 'app-itc',
  templateUrl: './itc.component.html',
  styleUrls: ['./itc.component.scss']
})
export class ItcComponent {

  public itcItem: Resourse;

  constructor(resoursesService: ResoursesService) {
    this.itcItem = resoursesService.getAllResourses()[1];
  }

}
