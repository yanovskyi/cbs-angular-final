import { Component, OnInit } from '@angular/core';
import { Resourse } from '../../models';
import { ResoursesService } from '../../services';

@Component({
  selector: 'app-itvdn',
  templateUrl: './itvdn.component.html',
  styleUrls: ['./itvdn.component.scss']
})
export class ItvdnComponent {

  public itvdnItem: Resourse;

  constructor(resoursesService: ResoursesService) {
    this.itvdnItem = resoursesService.getAllResourses()[0];
  }

}
