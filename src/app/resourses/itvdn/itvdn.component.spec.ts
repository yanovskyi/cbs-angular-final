import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItvdnComponent } from './itvdn.component';

describe('ItvdnComponent', () => {
  let component: ItvdnComponent;
  let fixture: ComponentFixture<ItvdnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItvdnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItvdnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
