import { Component, OnInit } from '@angular/core';
import { ResoursesService } from '../services';
import { Resourse } from '../models';

@Component({
  selector: 'app-resourses',
  templateUrl: './resourses.component.html',
  styleUrls: ['./resourses.component.scss']
})
export class ResoursesComponent implements OnInit {

  public resourses: Resourse[];

  constructor(resoursesService: ResoursesService) {

    this.resourses = resoursesService.getAllResourses();

  }

  ngOnInit() {

  }

}
